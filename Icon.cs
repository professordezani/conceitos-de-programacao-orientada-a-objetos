public class Icon : Component
{
    public string icon;

    public Icon(string icon)
    {
        this.icon = icon;
    }

     public override String GetValue() 
     {
        return icon;
     }
}