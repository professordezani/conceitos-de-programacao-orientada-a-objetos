﻿// Button with Text
Text txtSalvar = new Text("Salvar");
Console.WriteLine(txtSalvar.text);
Console.WriteLine(txtSalvar.id);

Component c1 = txtSalvar; // polimorfismo.
Console.WriteLine(c1.id);

Text t1 = (Text)c1;
Console.WriteLine(t1.text);

// Icon i1 = (Icon)c1;





Button btnSalvar = new Button(txtSalvar);
btnSalvar.OnClick();

// Button with Icon
Icon iconApagar = new Icon("Apagar");
Console.WriteLine(iconApagar.id);

Button btnApagar = new Button(iconApagar);
btnApagar.OnClick();















// Component c =  Component();

// using MeuProjeto;

// Button btnSalvar = new Button(new Text("Salvar"));
// Button btnApagar = new Button(new Text("Apagar"), "Red");

// Console.WriteLine(btnSalvar.text);
// Console.WriteLine(btnSalvar.color.ToUpper());

// Console.WriteLine(btnApagar.text);
// Console.WriteLine(btnApagar.color.ToUpper());

// btnSalvar.OnClick();
// btnApagar.OnClick();

// // Matematica mat = new Matematica();
// // int soma = mat.Soma(2, 3);
// // Console.WriteLine(soma);

// // int soma = Matematica.Soma(2, 3);

// // MeuProjeto.Math.Sqrt(20);
// // System.Math.Sqrt(30);