public class Button
{
    // atributos:
    public double width;
    public double height;
    public Component child; // Text ou Icon
    public string color;

    // construtor
    public Button(Component child)
    {
        this.child = child;
        this.color = "";
    }

    // construtor
    public Button(Component child, String color)
    {
        this.child = child;
        this.color = color;
    }

    public Button(Component child, double width)
    {
        this.child = child;
        this.width = width;
        this.color = "";
    }

    // método:
    public void OnClick()
    {
        Console.WriteLine($"O botão {this.child.GetValue()} foi pressionado.");
    }
}