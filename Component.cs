public abstract class Component
{
    public Guid id;

    public Component()
    {
        this.id = Guid.NewGuid();
    }

    public virtual String GetValue() 
    {
        return "";
    }
}