public class Text : Component
{
    // atributo:
    public string text;

    // construtor:
    public Text(string text)
    {
        this.text = text;
    }

     public override String GetValue()
     {
        return this.text;
     }
}